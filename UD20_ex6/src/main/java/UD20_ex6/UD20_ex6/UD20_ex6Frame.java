package UD20_ex6.UD20_ex6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class UD20_ex6Frame extends JFrame {

	private JPanel contentPane;
	private JTextField metros;
	private JTextField kg;
	private JTextField Resultado;

	//Main donde se ejecutará el Frame creado posteriormente
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UD20_ex6Frame frame = new UD20_ex6Frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Aquí creo el frame donde se ejecutará en el Main
	public UD20_ex6Frame() {
		//Aquí indico las configuraciones principales de mi ventana, como su tamaño, bordes, relleno
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		//Aquí creo una etiqueta indicando donde hay que poner la Altura
		JLabel lblNewLabel = new JLabel("Altura (metros)");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNewLabel, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNewLabel, 56, SpringLayout.WEST, contentPane);
		contentPane.add(lblNewLabel);
		//Aquí creo una etiqueta indicando donde hay que poner el Peso
		JLabel lblNewLabel_1 = new JLabel("Peso (kg)");
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNewLabel_1, 108, SpringLayout.EAST, lblNewLabel);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblNewLabel_1, 0, SpringLayout.SOUTH, lblNewLabel);
		contentPane.add(lblNewLabel_1);
		//Campo donde se ponen los metros
		metros = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, metros, 0, SpringLayout.NORTH, lblNewLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, metros, 4, SpringLayout.EAST, lblNewLabel);
		sl_contentPane.putConstraint(SpringLayout.EAST, metros, 60, SpringLayout.EAST, lblNewLabel);
		contentPane.add(metros);
		metros.setColumns(10);
		//Campo donde se ponen los kg
		kg = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, kg, 0, SpringLayout.NORTH, lblNewLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, kg, 6, SpringLayout.EAST, lblNewLabel_1);
		sl_contentPane.putConstraint(SpringLayout.EAST, kg, 62, SpringLayout.EAST, lblNewLabel_1);
		contentPane.add(kg);
		kg.setColumns(10);
		//Botón que hay que pulsar para calcular el indice de masa corporal
		JButton btnNewButton = new JButton("Calcular IMC");
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnNewButton, 20, SpringLayout.SOUTH, metros);
		sl_contentPane.putConstraint(SpringLayout.WEST, btnNewButton, -33, SpringLayout.WEST, metros);
		//Cuando el usuario haga click en el botón, se mostrará el resultado en el textField de la derecha de la 
		//etiqueta llamada IMC
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				double metrosDouble = Double.parseDouble(metros.getText());
				double kgDouble = Double.parseDouble(kg.getText());
				
				double resultadoFinal = (kgDouble/(Math.pow(metrosDouble,2)));
				
				Resultado.setText(String.valueOf(resultadoFinal));
			}
		});
		contentPane.add(btnNewButton);
		//Etiqueta donde muestra donde se mostrará el IMC
		JLabel lblNewLabel_2 = new JLabel("IMC");
		sl_contentPane.putConstraint(SpringLayout.EAST, btnNewButton, -29, SpringLayout.WEST, lblNewLabel_2);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 35, SpringLayout.SOUTH, lblNewLabel_1);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNewLabel_2, 0, SpringLayout.WEST, lblNewLabel_1);
		contentPane.add(lblNewLabel_2);
		//TextField donde se printará el resultado
		Resultado = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, Resultado, 20, SpringLayout.SOUTH, kg);
		sl_contentPane.putConstraint(SpringLayout.EAST, Resultado, 153, SpringLayout.EAST, lblNewLabel_2);
		Resultado.setEditable(false);
		sl_contentPane.putConstraint(SpringLayout.WEST, Resultado, 6, SpringLayout.EAST, lblNewLabel_2);
		contentPane.add(Resultado);
		Resultado.setColumns(10);
		//Aquí asigno el título de la App
		setTitle("Indice de Masa Corporal");
		
	}
}
